import Vue from 'vue'
import App from './App.vue'
import router from './router'
import Element from 'element-ui'
import store from './store'
import VCharts from 'v-charts'

import '@/assets/css/reset.css';
import '@/assets/css/element-variables.scss';
import '@/assets/css/coverElementCss.css';

Vue.use(Element, { zIndex: 3000})
Vue.use(VCharts);
Vue.config.productionTip = false

new Vue({
  router,
  store,
  render: h => h(App),
}).$mount('#app')
