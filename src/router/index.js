import Vue from 'vue';
import VueRouter from 'vue-router';
import { getUserInfo } from '@/data/common.js';
import AllRouter from './allRouter';

Vue.use(VueRouter);

const routes = [
    {
      path: '/login',
      name: 'login',
      component: () => import('@/views/login/login.vue')
    },
    ...AllRouter
]

const router = new VueRouter({
    routes
})

const routerMethods = ['push', 'replace']
routerMethods.forEach(method => {
  const originalCall = VueRouter.prototype[method]
  VueRouter.prototype[method] = function(location, onResolve, onReject) {
    if (onResolve || onReject) {
      return originalCall.call(this, location, onResolve, onReject)
    }
    return originalCall.call(this, location).catch(err => err)
  }
})

router.beforeEach((to, from, next) => {
    if (getUserInfo().isLogin){
      to.meta.isLogin = true;
      if(to.path === '/'){
        next({path:'/views/index'});
      }else{
        next();
      }
    }else{
      to.meta.isLogin = false;
      if(to.path !== '/login'){
        next({
          path: '/login'
        })
      }else{
        next();
      }
    }
});

export default router