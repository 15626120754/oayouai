export default [
    {
        path: '/views/',
        name: 'views',
        component: () => import('@/views'),
        children: [
            {
                path: 'index',
                component: () => import('@/views/home'),
                meta: []
            },
            {
                path: 'personnel', //人事管理
                component: () => import('@/views/personnelManagement/personnel/staffEntry'),
                meta: ['人事管理','员工入职']
            },
            //系统设置
            {
                path: 'menuSet',
                component: () => import('@/views/systemSet/menuSet'),
                meta: ['系统设置', '菜单管理']
            },
            {
                path: 'user',
                component: () => import('@/views/systemSet/user'),
                meta: ['系统设置', '用户管理']
            }
        ]
    }
]