//获取浏览器窗口的高度
const heightAuto = (h = 200) => {
    let height;
    height = (window.innerHeight || window.innerHeight)-h;
    return height;
}

export {
    heightAuto
}