import Cookies from 'js-cookie';
import moment from 'moment';

// 判断是否有用户信息
const getUserInfo = () => {
  let token = localStorage.getItem("token");
  if(!token){
    return {isLogin: false};
  }
  let userName = localStorage.getItem("userName");
  return {
    isLogin: true,
    userName,
    token
  }
}
  
//设置cookie
const setCookoes = (key, value) => {
  Cookies.set(key, value);
}
  
//获取cookie
const getCookoes = (key) => {
  let data = Cookies.get(key);
  if(data) return JSON.parse(data);
  return false;
}

const delay = (s = 500) => {
  return new Promise(succ => {
    setTimeout(() => {
      succ();
    }, s);
  })
}

const _loading = self => {
  return self.$loading({
    lock: true,
    spinner: 'el-icon-loading',
    background: 'rgba(0, 0, 0, 0.5)',
    text: '正在加载数据'
  });
}

// 格式化时间

const formatTime = (value, str = 'YYYY-MM-DD') =>{
  return moment(value).format(str);
}

export {
  getUserInfo,
  setCookoes,
  getCookoes,
  _loading,
  formatTime,
  delay
}