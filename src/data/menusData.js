/**
 * children子级
*/
export const MENUS = {
  admin: [{
    id: '1',
    name: '首页', 
    url: '/views/index'
  },{
    id: '2',
    name: '人事管理', 
    children: [{
      name: '员工入职',
      url: '/views/personnel'
    }]
  },{
    id: '3',
    name: '系统设置', 
    children: [{
      name: '菜单管理',
      url: '/views/menuSet'
    }]
  },
  /*菜单栏例子*/
  // {
  //   id: '8',
  //   name: '后台管理', 
  //   children: [{
  //     name: '管理员账户',
  //     url: '/admin/adminList'
  //   },{
  //     name: '权限设置',
  //     url: '/admin/allPower'
  //   },{
  //     name: '角色设置',
  //     url: '/admin/userList'
  //   },{
  //     name: '后台日志',
  //     url: '/admin/journal'
  //   },
    // {
    //   id: '10',
    //   name: '功能类',
    //   children: [
    //         {
    //           name: '详细鉴权',
    //           url: '/components/permission'
    //         },
    //         {
    //           name: '表格分页',
    //           url: '/components/pageTable'
    //         },
    //         {
    //           id: '1112',
    //           name: '功能类3',
    //           children: [
    //                 {
    //                   name: '详细鉴权2',
    //                   url: '/components/permission2'
    //                 },
    //                 {
    //                   name: '表格分页3',
    //                   url: '/components/pageTable3'
    //                 }
    //             ]
    //         }
    //     ]
    // }
  // ]
  // }
  ]
}