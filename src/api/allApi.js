import { service, path } from '@/api/request';

// 首页菜单获取
const homeList = () => {
  return service.get(`${path}/um/resources/homeList`);
}

//菜单查询
const list = () => {
  return service.get(`${path}/um/resources/list`);
}

//菜单添加
const addMenu = (params) => {
  return service.post(`${path}/um/resources`, {
    ...params
  });
}

//菜单编辑
const editMenu = (params) => {
  return service.put(`${path}/um/resources`, {
    ...params
  });
}

//菜单删除
const menuDelete = (params) => {
  return service.delete(`${path}/um/resources`,{
    data:{
      ...params
    }
  })
}

//用户管理列表
const userList = (params) => {
  return service.get(`${path}/um/user/pageList/1-1`,{
    data:{
      ...params
    }
  })
}

export {
  homeList,
  list,
  addMenu,
  editMenu,
  menuDelete,
  userList
}