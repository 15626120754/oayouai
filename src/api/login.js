import { service, path } from '@/api/request';



const login = (params) => {
  return service.post(`${path}/um/user/login`, params);
}

export {
  login
}