import { toFnu } from './request';

// 登录接口
import { login } from './login';

import {
  homeList,
  list,
  addMenu,
  editMenu,
  menuDelete,
  userList
} from './allApi.js';

export {
  toFnu,
  login,
  homeList,
  list,
  addMenu,
  editMenu,
  menuDelete,
  userList
}