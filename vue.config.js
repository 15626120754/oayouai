module.exports = {
  publicPath: process.env.NODE_ENV === 'production'
    ? ''
    : '/',
  devServer: {
    proxy: {
      '/api': {
        target: 'http://47.113.114.84',
        pathRewrite: {'^/api' : ''}
      },
      '/serveNameApi': {
        target: 'http://xxxx:8089',
        pathRewrite: {'^/serveNameApi' : ''}
      }
    }
  },
  css: {
    loaderOptions: {
      sass: {
        prependData: `@import "~@/assets/css/common.scss";`
      }
    }
  }
}